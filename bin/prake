#!/usr/bin/env bash

# shellcheck disable=SC1090

PROG_DIR="$(dirname "${BASH_SOURCE[0]}")"
LIB_DIR="$PROG_DIR/../share/prake"
. "$LIB_DIR/util/standard-vars.sh"
. "$LIB_DIR/util/standard-funcs.sh"
. "$LIB_DIR/util/standard-help-version.sh"

trap _sig_recv SIGINT

ALIAS_FILE=
INIT_DIR="."
DIRS_STRING=
LANG_STRING=
PROJECT_NAME="project"

SED=

_init() {
	ALIAS_FILE=$PRAKE_ALIAS_FILE
	[[ -z "$ALIAS_FILE" ]] && ALIAS_FILE="$HOME/.config/prake/aliases"
	_check_file_exists "$ALIAS_FILE" y
	# Damn MacOs
	SED="$(command -v sed)"
	GSED="$(command -v gsed)"
	[[ -n "$GSED" ]] && SED="$GSED"
}

func_alias() {
	_write_to_list() {
		ALIAS="$1"; shift
		CONTENT=$({ grep -v "^$ALIAS=" "$ALIAS_FILE" || echo ""; } \
			| "$SED" "$ a$ALIAS=$*" \
			| "$SED" '/^$/d')
		echo "$CONTENT" > "$ALIAS_FILE"
	}
	_get_from_list() {
		LINE=$(grep "^$1=" "$ALIAS_FILE" \
			| "$SED" "s/^$1=//")
		if [[ -z "$LINE" ]]; then
			_msg_ext "Alias $1 not found" "$EOPTN"
		fi
		echo "$LINE"
	}
	_msg_dbg "w/ $*"
	while [[ -n $1 ]]; do
		case "$1" in
			-l|--list)
				_msg_dbg "Listing $ALIAS_FILE"
				cat "$ALIAS_FILE" ;;
			-*) 
				_msg_ext "Option $1 not found for dirs" "$EOPTN" ;;
			*) 
				ALIAS="$1"; shift
				if [[ -n "$1" ]]; then
					 _write_to_list "$ALIAS" "$*"
					 argv=("$@")
				else
					IFS=' ' read -r -a argv <<< "$(_get_from_list "$ALIAS")"
				fi
				break
		esac
		shift
	done
	[[ -n "$1" ]] && _argparse "${argv[@]}"
}

func_dirs() {
	_msg_dbg "w/ $*"
	virk_dirname="$($(command -v nvim) --headless "+echo g:virk_dirname" +qall 2>&1 | head -1)"
	[[ $virk_dirname =~ ^Error ]] && virk_dirname=".virkspace"
	virk_dirname=${virk_dirname%%[[:space:]]}
	GIT=0; VIRK=0
	while [[ -n $1 ]]; do
		case "$1" in
			-g|--git) 
				GIT=1; _msg_dbg "Git enabled" ;;
			-v|--virkspace) 
				VIRK=1; _msg_dbg "Virk enabled" ;;
			-a|--additional)
				DIRS_STRING="$DIRS_STRING mkdir -p ${2//,/ };"
				_msg_dbg "Adding $1"
				shift ;;
			-b|--basepath)
				INIT_DIR="$2"
				_msg_dbg "Will move into $2"; shift ;;
			-*) _msg_ext "Option $1 not found for dirs" "$EOPTN" ;;
			*) break
		esac
		shift
	done
	if [[ "$GIT" == 1 ]]; then
		DIRS_STRING="$DIRS_STRING git init; touch .gitignore README.md;"
	fi 
	if [[ "$VIRK" == 1 ]]; then
		DIRS_STRING="$DIRS_STRING mkdir $virk_dirname;"
		[[ "$GIT" == 1 ]] && DIRS_STRING="$DIRS_STRING \
			echo $virk_dirname >> .gitignore; \
			echo .virkignore >> .gitignore;"
	fi
	[[ -n "$1" ]] && _argparse "$@"
}

func_python() {
	_msg_dbg "w/ $*"
	VENV=
	PYBIN=$(command -v python)
	REVISION="3"
	while [[ -n "$1" ]]; do
		case "$1" in
			-p|--path)
				[[ -z "$2" ]] && _msg_ext "No path supplied for Python (-p)" "$EOPTN"
				PYBIN="$2"
				[[ -x "$PYBIN" ]] ||
					_msg_ext "$PYBIN not executable" "$EOPTN" 
				shift ;;
			-r|--revision)
				REVISION="$2"; shift
				PYBIN="$(command -v "python$REVISION")" || \
					_msg_ext "No Python, revision $REVISION, on PATH" "$EOPTN" ;;
			-v|--venv)
				VENV=" -m venv .; . ./bin/activate;"
				true ;;
			-*) _msg_ext "Option $1 not found for python" "$EOPTN" ;;
			*) break
		esac
		shift
	done
	if [[ -n "$VENV" ]]; then
		DIRS="mkdir -p $PROJECT_NAME/src \
			&& cd $PROJECT_NAME && echo \#\!/usr/bin/env python$REVISION > src/$PROJECT_NAME \
			&& chmod +x src/$PROJECT_NAME"
		LANG_STRING="$LANG_STRING $PYBIN $VENV $DIRS;"
	else
		DIRS="mkdir src && echo \#\!$PYBIN > src/$PROJECT_NAME \
			&& chmod +x src/$PROJECT_NAME"
		LANG_STRING="$LANG_STRING $DIRS;"
	fi
	[[ -n "$1" ]] && _argparse "$@"
}

func_golang() {
	_msg_dbg "w/ $*"
	DEP=""; MOD=1
	PACKAGE=
	GOPATH="$(go env GOPATH)" || _msg_ext "Go not found on PATH" "$EOPTN"
	while [[ -n "$1" ]]; do
		case "$1" in
			-g|--gopath)
				[[ -n "$2" ]] || _msg_ext "No path supplied for GOPATH (-g)" "$EOPTN"
				GOPATH="$2"
				shift ;;
			-p|--package)
				[[ -n "$2" ]] || _msg_ext "No package name supplied for GOPATH (-p)"  "$EOPTN"
				PACKAGE="$2"
				shift ;;
			-d|--dep)
				_check_dir_exists "$GOPATH/src"
				DEP=$(command -v dep)
				MOD=
				INIT_DIR="$GOPATH/src;" ;;
			-*) _msg_ext "Option $1 not found for golang" "$EOPTN" ;;
			*) break
		esac
		shift
	done
	[[ -n "$PACKAGE" ]] && {
		[[ -n "$DEP" ]] && LANG_STRING="$LANG_STRING mkdir -p $PACKAGE && cd $PACKAGE; dep init;"
		[[ -n "$MOD" ]] && LANG_STRING="$LANG_STRING go mod init $PACKAGE;"
	}
	[[ -n "$1" ]] && _argparse "$@"
}

func_bash() {
	_msg_dbg "/w $*"
	GIT_DIR="$HOME/gitclones/benjcs-bash-boilerplate/"
	while [[ -n "$1" ]]; do
		case "$1" in
			-d|--defaults)
				if [[ ! -d "$GIT_DIR" ]]; then
					cd "$HOME/gitclones" || _exit_msg "$HOME/gitclones cannot be reached" "$EPROG"
					git clone https://gitlab.com/BodneyC/benjcs-bash-boilerplate.git
					cd - || _exit_msg "Original dir cannot be reached" "$EPROG"
				fi
				mkdir -p util
				ln -s "$GIT_DIR/scripts/standard-vars.sh" "util/standard-vars.sh"
				ln -s "$GIT_DIR/scripts/standard-funcs.sh" "util/standard-funcs.sh"
				cp "$GIT_DIR/scripts/standard-help-version.sh" "utils/" ;;
			-*) _msg_ext "Option $1 not found for golang" "$EOPTN" ;;
			*) break
		esac
		shift
	done
	LANG_STRING="$LANG_STRING touch $PROJECT_NAME.sh;"
	[[ -n "$1" ]] && _argparse "$@"
}

###
# NOTE: The spring stuff, perhaps this whole function, just shouldn't exist, 
#        shame on me for doing this, just use initializr...
# 		It does work though.
###
func_jvm() {
	_2_check() {
		[[ -z "$1" ]] && _msg_ext "Argument missing for spring" "$EOPTN"
	}
	_make_spring_url() {
		SPRING_URL="https://start.spring.io/starter.zip?a=b"
		[[ -n ${SPRING_OPTS["type"]} ]]         && SPRING_URL="$SPRING_URL&type=${SPRING_OPTS["type"]}"
		[[ -n ${SPRING_OPTS["language"]} ]]     && SPRING_URL="$SPRING_URL&language=${SPRING_OPTS["language"]}"
		[[ -n ${SPRING_OPTS["version"]} ]]      && SPRING_URL="$SPRING_URL&bootVersion=${SPRING_OPTS["version"]}"
		[[ -n ${SPRING_OPTS["name"]} ]]         && SPRING_URL="$SPRING_URL&name=${SPRING_OPTS["name"]}"
		[[ -n ${SPRING_OPTS["description"]} ]]  && SPRING_URL="$SPRING_URL&description=${SPRING_OPTS["description"]}"
		[[ -n ${SPRING_OPTS["package"]} ]]      && SPRING_URL="$SPRING_URL&packageName=${SPRING_OPTS["package"]}"
		[[ -n ${SPRING_OPTS["packaging"]} ]]    && SPRING_URL="$SPRING_URL&packaging=${SPRING_OPTS["packaging"]}"
		[[ -n ${SPRING_OPTS["baseDir"]} ]]      && SPRING_URL="$SPRING_URL&baseDir=${SPRING_OPTS["baseDir"]}"
		[[ -n ${SPRING_OPTS["groupId"]} ]]      && SPRING_URL="$SPRING_URL&groupId=${SPRING_OPTS["groupId"]}"
		[[ -n ${SPRING_OPTS["artifactId"]} ]]   && SPRING_URL="$SPRING_URL&artifactId=${SPRING_OPTS["artifactId"]}"
		[[ -n ${SPRING_OPTS["javaversion"]} ]]  && SPRING_URL="$SPRING_URL&javaVersion=${SPRING_OPTS["javaversion"]}"
		for d in ${SPRING_OPTS["dependencies"]}; do
			SPRING_URL="$SPRING_URL&dependencies=$d"
		done
		echo "$SPRING_URL"
	}
	_msg_dbg "w/ $*"
	MVN=0; GRADLE=0; SPRING=0; PACKAGE=
	while [[ -n "$1" ]]; do
		case "$1" in
			-p|--package)
				[[ "$SPRING" == 1 ]] && _msg_ext "Specify package in --spring-package" "$EOPTN"
				[[ -n "$2" ]] || _msg_ext "No package name supplied for JVM (-p)"  "$EOPTN"
				PACKAGE="$("$SED" 's/\./\//g' <<< "$2")" ;;
			-g|--gradle)
				[[ "$SPRING" == 1 ]] && _msg_ext "Specify project type in --spring-type" "$EOPTN"
				[[ "$MVN" == 1 ]] && _msg_ext "Cannot specify both Gradle and Maven" "$EOPTN"
				GRADLE=1; _2_check "$2"; 
				PACKBIN="$(command -v gradle)" || _msg_ext "Cannot find gradle on PATH" "$EOPTN"
				ARCHETYPE="--init-script $2" ;;
			-m|--mvn|--maven)
				[[ "$SPRING" == 1 ]] && _msg_ext "Specify project type in --spring-type" "$EOPTN"
				[[ "$GRADLE" == 1 ]] && _msg_ext "Cannot specify both Gradle and Maven" "$EOPTN"
				MVN=1; _2_check "$2";
				PACKBIN="$(command -v mvn)" || _msg_ext "Cannot find gradle on PATH" "$EOPTN"
				ARCHETYPE="archetype:generate $2" ;;
			-s|--spring)
				[[ "$MVN" == 1 || "$GRADLE" == 1 || "$PACKAGE" == 1 ]] \
					&& _msg_ext "Specify project name and type in --spring-*" "$EOPTN"
				SPRING=1; shift
				declare -A SPRING_OPTS
				while [[ -n "$1" && "$1" =~ (--spring-|-s-) ]]; do
					OPT="${1//--spring-/}"
					OPT="${OPT//-s-/}"
					case "$OPT" in
						t|type)               _2_check "$2"; SPRING_OPTS["type"]="$2"            ;;
						l|lang|language)      _2_check "$2"; SPRING_OPTS["language"]="$2"        ;;
						v|vers|version)       _2_check "$2"; SPRING_OPTS["version"]="$2"         ;;
						n|name)               _2_check "$2"; SPRING_OPTS["name"]="$2"            ;;
						d|desc|description)   _2_check "$2"; SPRING_OPTS["description"]="$2"     ;;
						p|package)            _2_check "$2"; SPRING_OPTS["package"]="$2"         ;;
						packaging)            _2_check "$2"; SPRING_OPTS["packaging"]="$2"       ;;
						base[dD]ir)           _2_check "$2"; SPRING_OPTS["baseDir"]="$2"         ;;
						g[iI]d|group[iI]d)    _2_check "$2"; SPRING_OPTS["groupId"]="$2"         ;;
						a[iI]d|artifact[iI]d) _2_check "$2"; SPRING_OPTS["artifactId"]="$2"      ;;
						j|java[Vv]ersion)     _2_check "$2"; SPRING_OPTS["javaversion"]="$2"     ;;
						deps?|dependencies)   _2_check "$2"; SPRING_OPTS["dependencies"]+=("$2") ;;
						*) _msg_ext "Option $OPT not found for Spring" "$EOPTN" ;;
					esac
					shift; shift
				done ;;
			-*) _msg_ext "Option $1 not found for jvm" "$EOPTN" ;;
			*) break
		esac
		shift
	done
	if [[ -n "$PACKBIN" ]]; then
		LANG_STRING="$LANG_STRING $PACKBIN $ARCHETYPE;"
		[[ -n "$PACKAGE" ]] && LANG_STRING="$LANG_STRING mkdir -p $PACKAGE;"
	elif [[ "$SPRING" == 1 ]]; then
		SPRING_GET=$(_make_spring_url "${SPRING_OPTS[@]}")
		LANG_STRING="$LANG_STRING curl -xGET $SPRING_GET;"
	fi
	[[ -n "$1" ]] && _argparse "$@"
}

func_nodejs() {
	_msg_dbg "w/ $*"
	NPM=0; YARN=0; NODEBIN=""; AUTO="yes '' | "
	while [[ -n "$1" ]]; do
		case "$1" in
			-b|--bin)
				[[ -z "$2" ]] && _msg_ext "No path supplied for Yarn/NPM (-p)" "$EOPTN"
				NODEBIN="$2"
				[[ -x "$NODEBIN" ]] ||
					_msg_ext "$NODEBIN not executable" "$EOPTN" 
				shift ;;
			-n|--npm)
				[[ "$YARN" == 1 ]] && _msg_ext "Cannot specify both NPM and Yarn" "$EOPTN"
				NPM=1
				NODEBIN="$(command -v npm)" || \
					_msg_ext "Cannot find NPM on PATH" "$EOPTN";;
			-y|--yarn)
				[[ "$NPM" == 1 ]] && _msg_ext "Cannot specify both NPM and Yarn" "$EOPTN"
				YARN=1
				NODEBIN="$(command -v yarn)" || \
					_msg_ext "Cannot find Yarn on PATH" "$EOPTN";;
			-a|--auto) AUTO="" ;;
			-*) _msg_ext "Option $1 not found for nodejs" "$EOPTN" ;;
			*) break
		esac
		shift
	done
	[[ -n "$NODEBIN" ]] && LANG_STRING="$LANG_STRING $AUTO $NODEBIN init; mkdir src && touch src/index.js;"
	[[ -n "$1" ]] && _argparse "$@"
}

project_init() {
	TMP_SCRIPT="$(mktemp)"
	chmod +x "$TMP_SCRIPT"
	echo "cd $INIT_DIR" > "$TMP_SCRIPT"
	echo "$LANG_STRING" >> "$TMP_SCRIPT"
	echo "$DIRS_STRING" >> "$TMP_SCRIPT"
	"$TMP_SCRIPT" || _msg_ext "Script ($TMP_SCRIPT) failed: $(cat "$TMP_SCRIPT")" "$EPROG"
}

main() {
	_init
	. "$LIB_DIR/argparsers/main.sh"
	_argparse "$@"
	project_init
}

main "$@"
