#!/usr/bin/env bash

###
# FN: sub-command-pattern.sh
# DS: A configurable sub-command pattern for argument 
#      parsing in bash. One would need to set the cases 
#      to something a little more useful than command1, 
#      2, 3... to actually use this.
#     The idea is to have one of these files for each
#      subcommand and source the relevant file redefining
#      the _argparse() function, each time executing
#      func_$cmd with the args and switches.
###

_find_next_subcommand() {
	while [[ -n $1 ]]; do
		if ! [[ "$1" =~ ^-.* ]]; then
			echo "$@"
			return
		fi
		shift
	done
}

shopt -s extglob
_argparse() {
	_argparse_done_check() {
		[[ -z "$1" && -z "$2" ]] && echo 1
	}

	argv=()
	cmd=""
	arg_end=""

	[[ $# == 0 ]] && cmd="help"
 
	while [[ -n "$1" ]]; do
		CHECK=$(_argparse_done_check "$cmd" "$arg_end")
		case "$1" in
			--) arg_end=1 ;;
			-*) 
				# shellcheck disable=SC2034
				case "$1" in
					-h)           [[ -n "$CHECK" ]] && cmd="help"    || argv+=("$1") ;;
					-H|--help)    [[ -n "$CHECK" ]] && cmd="HELP"    || argv+=("$1") ;;
					-v)           [[ -n "$CHECK" ]] && cmd="version" || argv+=("$1") ;;
					-V|--version) [[ -n "$CHECK" ]] && cmd="VERSION" || argv+=("$1") ;;
					-d|--debug)   if [[ "$DEBUG" == 0 ]]; then
									DEBUG=1; _msg_dbg "Debug on"
								  else
									argv+=("$1")
								  fi ;;
					-n|--name)    if [[ -n "$CHECK" ]]; then
									PROJECT_NAME="$2"; _msg_dbg "Project name set to $2"; shift
								  else
									argv+=("$1")
								  fi ;;
					*)            [[ -n "$CHECK" ]] \
									&& _msg_ext "Unknown option: $1" "$EOPTN" \
									|| argv+=("$1")
				esac ;;
			?(ba)sh)    [[ -n "$CHECK" ]] && cmd="bash"    || argv+=("$1") ;;
			d?(i)r?(s)) [[ -n "$CHECK" ]] && cmd="dirs"    || argv+=("$1") ;;
			py?(thon))  [[ -n "$CHECK" ]] && cmd="python"  || argv+=("$1") ;;
			go?(lang))  [[ -n "$CHECK" ]] && cmd="golang"  || argv+=("$1") ;;
			jvm)        [[ -n "$CHECK" ]] && cmd="jvm"     || argv+=("$1") ;;
			n?(ode)js)  [[ -n "$CHECK" ]] && cmd="nodejs"  || argv+=("$1") ;;
			al?(ias))   [[ -n "$CHECK" ]] && cmd="alias"   || argv+=("$1") ;;
			*)          [[ -n "$CHECK" ]] && cmd="$1"      || argv+=("$1") ;;
		esac
		shift
	done

	if ! type "func_$cmd" >& /dev/null; then
		_msg_ext "Unknown command: func_$cmd" "$EOPTN"
	fi

	"func_$cmd" "${argv[@]}"
}
