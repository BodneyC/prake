#!/usr/bin/env bash

###
# FN: standard-help-version.sh
# DS: Provides help and version functionality 
#      in the manner which is default for 
#      sub-command-pattern of the same 
#      repository
###

func_help() {
	printf "%s\n" "prake [options,] [(bash|dirs|golang|jvm|nodejs|python) [options,],]"
}

func_HELP() {
    cat << EOF >> /dev/stdout
  prake [options,] [(bash|dirs|golang|jvm|nodejs|python) [options,],]

    -d|--debug)    -- Enables debuggin
    -h|--help)     -- Show help information
    -n|--name)     -- Project name
    -v|--version)  -- Show version information

    bash
      -d|--defaults)  -- Download https://github.com/BodneyC/benjcs-bash-boilerplate.git
                          and copy across

    dirs
      -a|--additional  -- Comma separated list of dirs to make
      -g|--git         -- Git init
      -p|--path        -- Starting directory
      -v|--virkspace   -- Create VirkSpace

    golang
      --gopath   -- \$GOPATH / directory of choice
      --package  -- Package name (e.g. world.hello)

    jvm
      -g|--gradle   -- Use Gradle
      -m|--maven    -- Use Maven
      -p|--package  -- Package name (e.g. world.hello)
      -s|--spring   -- Spring Initializr
         --spring-(t|type)                -- Maven or Gradle
         --spring-(l|lang|language)       -- Language support
         --spring-(v|vers|version)        -- SpringBoot version
         --spring-(n|name)                -- Project name
         --spring-(d|desc|description)    -- Project description
         --spring-(p|package)             -- 
         --spring-packaging               -- JAR/WAR
         --spring-base[dD]ir              -- Base directoru
         --spring-(g[iI]d|group[iI]d)     -- Group Id
         --spring-(a[iI]d|artifact[iI]d)  -- Artifact Id
         --spring-(j|java[Vv]ersion)      -- Java language version
         --spring-(deps?|dependencies)    -- Additional dependencies (specify multiple times)

    nodejs
      --npm   -- Use NPM
      --path  -- Path to npm/yarn
      --yarn  -- Use Yarn

    python
      --path      -- Path to Python binary
      --revision  -- Python revision (e.g. 3.7)
      --venv      -- Create a virtual environment

EOF
}

func_version() {
	printf "%s\n" "0.0.1"
}

func_VERSION() {
	cat << EOF >> /dev/stdout
Copyright © 2019 Prake - Benjamin Carrington
 
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.
 
Version: 0.0.1
EOF
}

