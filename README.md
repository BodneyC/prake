Prake
=====

Project-make, heavily over-engineered Bash (4+) script to generate baseline projects to my particular taste/requirements.

If you're a ZSH user then a completions script is provided.

## Installation

    ./install.sh

This does assume that you're using [oh my zsh](https://github.com/robbyrussell/oh-my-zsh) where the completions are concerned.

Also, assumes `$HOME/.local/bin` is on your path.

Really the install script isn't too thorough as it was late in the day when I wrote it.

## Functionality

This is best explained with the `--help` flag:

	  prake [options,] [(bash|dirs|golang|jvm|nodejs|python) [options,],]

		-d|--debug)    -- Enables debuggin
		-h|--help)     -- Show help information
		-n|--name)     -- Project name
		-v|--version)  -- Show version information

		bash
		  -d|--defaults)  -- Download https://github.com/BodneyC/benjcs-bash-boilerplate.git
							  and copy across

		dirs
		  -a|--additional  -- Comma separated list of dirs to make
		  -g|--git         -- Git init
		  -p|--path        -- Starting directory
		  -v|--virkspace   -- Create VirkSpace

		golang
		  --gopath   -- $GOPATH / directory of choice
		  --package  -- Package name (e.g. world.hello)

		jvm
		  -g|--gradle   -- Use Gradle
		  -m|--maven    -- Use Maven
		  -p|--package  -- Package name (e.g. world.hello)
		  -s|--spring   -- Spring Initializr
			 --spring-(t|type)                -- Maven or Gradle
			 --spring-(l|lang|language)       -- Language support
			 --spring-(v|vers|version)        -- SpringBoot version
			 --spring-(n|name)                -- Project name
			 --spring-(d|desc|description)    -- Project description
			 --spring-(p|package)             -- 
			 --spring-packaging               -- JAR/WAR
			 --spring-base[dD]ir              -- Base directoru
			 --spring-(g[iI]d|group[iI]d)     -- Group Id
			 --spring-(a[iI]d|artifact[iI]d)  -- Artifact Id
			 --spring-(j|java[Vv]ersion)      -- Java language version
			 --spring-(deps?|dependencies)    -- Additional dependencies (specify multiple times)

		nodejs
		  --npm   -- Use NPM
		  --path  -- Path to npm/yarn
		  --yarn  -- Use Yarn

		python
		  --path      -- Path to Python binary
		  --revision  -- Python revision (e.g. 3.7)
		  --venv      -- Create a virtual environment


