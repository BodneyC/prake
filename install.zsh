#!/usr/bin/env zsh

cd "$(dirname "${(%):-%N}")" || exit

COMPLETIONS_DIR="$ZDOTDIR"

if [[ ! -d "$COMPLETIONS_DIR" ]]; then
	echo "Could not find $COMPLETIONS_DIR, look in your \$fpath and find a good place to put _prake"
else
	[[ ! -d "$COMPLETIONS_DIR/completions" ]] && mkdir "$COMPLETIONS_DIR/completions"
	cp ./completions/_prake "$COMPLETIONS_DIR/completions/_prake"
	autoload -U compinit
	compinit
	autoload -U _prake
fi

mkdir -p ~/.local/{bin,share/prake/{util,argparsers}}

cp ./bin/prake "$HOME/.local/bin/prake"
cp ./share/prake/util/* "$HOME/.local/share/prake/util/"
cp ./share/prake/argparsers/main.sh "$HOME/.local/share/prake/argparsers/"
